const { request, setup, teardown } = require('../../utils/setup-teardown-common');
const { Member } = require('../../../app/models');

beforeEach(setup);
afterEach(teardown);

describe('Integration::Members::DeleteMember', () => {
  test('noop', async () => {
  });
});
