module.exports = {
  lastName: {
    french: 'Nom de famille',
  },
  firstName: {
    french: 'Prénom',
  },
  description: {
    french: 'Description',
  },
  email: {
    french: 'Adresse email',
  },
  phone: {
    french: 'Téléphone portable',
  },
  stageName: {
    french: 'Nom de scène',
  },
  musicStyle: {
    french: 'Style de musique',
  },
  whereListen: {
    french: 'Où écouter ?',
  },
  association: {
    french: 'Association',
  },
  startDate: {
    french: 'Date de début',
  },
  endDate: {
    french: 'Date de fin',
  },
  objectName: {
    french: 'Nom de l\'objet',
  },
  lostDate: {
    french: 'Date de perte',
  },
  pageName: {
    french: 'Nom de la page web',
  },
};
