import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Barman, Service, Task } from '../_models';
import { Moment } from 'moment';
import { Observable } from 'rxjs';

@Injectable()
export class BarmanService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Barman[]> {
    return this.http.get<Barman[]>('/api/barmen');
  }

  getById(id: number): Observable<Barman> {
    return this.http.get<Barman>(`/api/barmen/${id}`);
  }

  getServices(id: number, start: Moment, end: Moment): Observable<Service[]> {
    return this.http.get<Service[]>(`/api/barmen/${id}/services`, {
      params: {
        startAt: (+start).toString(),
        endAt: (+end).toString(),
      },
    });
  }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>('/api/me/tasks');
  }

  create(barman: Barman): Observable<Barman> {
    return this.http.post<Barman>('/api/barmen', barman);
  }

  addService(id: number, services: number[]): Observable<Service> {
    return this.http.post<Service>(`/api/barmen/${id}/services`, services);
  }

  removeService(id: number, services: number[]): Observable<Service> {
    return this.http.post<Service>(`/api/barmen/${id}/services/delete`, services);
  }

  update(barman: Barman): Observable<Barman> {
    return this.http.put<Barman>(`/api/barmen/${barman.id}`, barman);
  }

  delete(id: number): Observable<Barman> {
    return this.http.post<Barman>(`/api/barmen/${id}/delete`, null);
  }
}
